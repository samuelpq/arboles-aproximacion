#!/usr/bin/env pythoh3


import sys

base_trees = ""
fruit_per_tree = ""
reduction = ""


def compute_trees(trees):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees):
    productions = []
    best_production = float('-inf')
    best_trees = 0

    for trees in range(min_trees, max_trees + 1):
        production = compute_trees(trees)
        productions.append((trees, production))

        if production > best_production:
            best_production = production
            best_trees = trees

    return productions, best_trees, best_production


def read_arguments():
    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit(1)

    try:
     base_trees = int(sys.argv[1])
     fruit_per_tree = int(sys.argv[2])
     reduction = int(sys.argv[3])
     min_trees = int(sys.argv[4])
     max_trees = int(sys.argv[5])

    except ValueError:
        print("All arguments must be integers")
        sys.exit(1)

    return base_trees, fruit_per_tree, reduction, min_trees, max_trees


def main():
    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()
    productions, best_trees, best_production = compute_all(min_trees, max_trees)

    for trees, production in productions:
        print(f"{trees} {production}")

    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    main()
